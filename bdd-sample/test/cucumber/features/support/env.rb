require 'orange_lib'

Observation_hash = Hash.new
Entity_hash = Hash.new
Index_hash = Hash.new

def app
  @__app ||= OrangeLib::Rest.new({
                                  base_uri: "#{ENV['esEndPoint']}",
                                  content_type: 'application/json',
                                  timeout: 60,
                                  debug_request: 'true'
                              })
  @__app
end
Before do |scenario|
  # The +scenario+ argument is optional, but if you use it, you can get the title,
  # description, or name (title + description) of the scenario that is about to be
  # executed.
  puts "Starting scenario: #{scenario.name}"
end

After do |scenario|
  # Do something after each scenario.
  # The +scenario+ argument is optional, but
  # if you use it, you can inspect status with
  # the #failed?, #passed? and #exception methods.

  if scenario.failed?
    puts "End Scenario: #{scenario.name}"
  end

  if scenario.passed?
    puts "End Scenario: #{scenario.name}"
  end
end
require 'orange_lib'
require 'json'

Given /^On (Elastic Search)$/ do |server|
  app.setHost "#{ENV['esEndPoint']}"
  puts app.Host
end

When(/^user navigates to some host$/) do
  app.setHost "#{ENV['cassandraEndPoint']}"
  puts "Hello, we are in #{app.Host} now"
end

When(/^user set variable "([^"]*)" with value equal "([^"]*)"$/) do |arg1, arg2|
  app.variable(arg1, arg2)
  puts "You have just created variable #{arg1} with value #{app.variable(arg2)}"
end

When(/^user need to use use the below string$/) do |text|
  puts "You get string with multiple lines from feature file:\n #{text}"
end

When(/^user setup the table by row$/) do |table|
  # table is a table.hashes.keys # => [:alertId]
  params = table.rows_hash

  params.each do |key, value|
    puts "#{params[key]} and value=#{params[value]} \n"
  end
end

When(/^user setup the table by column$/) do |table|
  # table is a table.hashes.keys # => [:RESPONSE_CODE, :RESPONSE_ACTION_CODE, :RESPONSE_MESSAGE]
  params = table.hashes

  params.each do |ac_row|
    ac_row.each do |key,value|
      puts "#{ac_row[key]} and value=#{ac_row[value]}"
    end
    puts "\n"
  end
end

When(/^user needs to interpret string$/) do
  app.variable('@var1',12345)
  puts app.variable('@var1')
  s = 'You just created variable @var1 with value %{@var1}'
  puts app.interpret_string(s)
end

When(/^user chooses a value (foo|solid|zero)$/) do |chosen|
  puts "you choose #{chosen}"
end

When(/^number (\d+) plus number (\d+)$/) do |num1, num2|
  app.variable('@sum',(num1.to_i + num2.to_i))
end

When(/^user needs to call step$/) do
  step 'number 5 plus number 5'
  puts app.variable('@sum')
end

When(/^user want to send post request$/) do
  file_path = 'data/entity.json'
  body = File.read(file_path)

  #body = "{\"practitioner\": {\"address1\": \"319 Binh Gia\",\"address2\": \"601 CMT8\"}}"
  body_hash = JSON.parse(body)

  #create options
  opts = Hash.new

  #init request body
  opts[:body] = body_hash.to_json

  # set request header
  h = Hash.new
  key = 'content-type'
  value = 'application/json'
  h[key] = value
  key = 'accept'
  h[key] = value
  app.headers(h)

  app.perform_request 'put', '/sampleindex/entity/PIIDAUTO00001', opts
  puts app.response.body
end

When(/^user want to send get request$/) do
  app.perform_request 'get', '/sampleindex/entity/PIIDAUTO00001', {}
  puts app.response.body
end


Then(/^user verifies the response$/) do
  json = JSON.parse(app.response.body)
  json_path = '$._source.practitioner.hms_piid'
  results = JsonPath.new(json_path).on(json).to_a.map(&:to_s)

  puts "#{results}"
  expect(results).to include("PIIDAUTO00001")
end

Given(/^(.*) without a table$/) do |outline_value|
  puts "#{outline_value}"
end
Feature:  <Jira-Ticket>: Summary name
  In order to keep track of stock
  As a store owner
  I want to add items back to stock when they are returned

  Background:
    Given On Elastic Search

  Scenario: <Scenario ID> - sample scenario
    When user navigates to some host
    Given On Elastic Search
    When user set variable "@var1" with value equal "foo"
    When user needs to interpret string
    When user chooses a value foo
    When user need to use use the below string
    """
      I'm string with multiple lines
      Please get me in your step_definition
    """
    When user setup the table by row
      | alertId                   |   789                     |
      | customerCode              |   CSTAUTO                 |
      | enabled                   |   true                    |
      | controlledSubstance       |   true                    |
      | priority                  |   1                       |
      | responseActionCode        |   INFO                    |
      | responseMessage           |   Testing                 |
    When user setup the table by column
      |   RESPONSE_CODE         |   RESPONSE_ACTION_CODE      | RESPONSE_MESSAGE        |
      |   CSTAUT1               |   REJECT                    | Testing                 |
      |   CSTAUT2               |   REJECT                    | Testing                 |
      |   CSTAUT3               |   REJECT                    | Testing                 |
    When user needs to call step
    When user want to send post request
    When user want to send get request
    Then user verifies the response

  Scenario Outline: Test state
    Given <state> without a table
    Given <other_state> without a table
    Examples: Rainbow colours
      | state    | other_state |
      | missing  | passing     |
      | passing  | passing     |
      | failing  | passing     |
    Examples:Only passing
      | state    | other_state |
      | passing  | passing     |